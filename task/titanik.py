import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    values = []
    titles = ["Mr.", "Mrs.", "Miss."]
    for title in titles:
        d = df[df.Name.str.contains(title)]
        m = d.Age.median()
        c = d[df.Age.isna()].count()
        values.append((title, c, int(round(m))))
    return values
